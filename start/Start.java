package start;

import business.TasksResultProcessor;

public class Start {
	
	private static TasksResultProcessor processor = new TasksResultProcessor();
	private static String inputFileName = "Activities.txt";
	
	public static void main(String[] args) {
		
		processor.task1(inputFileName);
		processor.task2();
		processor.task3();
		processor.task4();
		processor.task5();
		processor.task6();
		
		System.out.println("Done!");
	}

}
