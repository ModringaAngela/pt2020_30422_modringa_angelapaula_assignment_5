package business;

import data.MonitoredData;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.stream.*;
import java.util.List;
import java.util.Map;
import java.lang.*;

public class TasksPerformer {
	
	
	private static List<String> split(String str, String pattern){
	    return Stream.of(str.split(pattern))
	      .map (elem -> new String(elem))
	      .collect(Collectors.toList());
	}
	
	private static List<Integer> parse(List<String> list){
		return list.stream()
				.map(s-> Integer.parseInt(s))
				.collect(Collectors.toList());
	}
	
	public List<MonitoredData> doTask1(String inputFileName) {
		
		List<MonitoredData> activities = new ArrayList<MonitoredData>();
		try {
			InputStream stream = new FileInputStream(inputFileName); 
			InputStreamReader streamReader = new InputStreamReader(stream);
			BufferedReader buffer = new BufferedReader(streamReader);
			String line = null;
			while((line = buffer.readLine()) != null) {
				try {
					List<String> lineComponents = split(line,  "		");
					List<String> dateComponents = split(lineComponents.get(0), " ");
					List<String> timeComponents = split(lineComponents.get(1), " ");
					List<Integer> date = parse(split(dateComponents.get(0), "-"));
					List<Integer> time = parse(split(dateComponents.get(1), ":"));
					
					LocalDateTime start = LocalDateTime.of(date.get(0), date.get(1), date.get(2),
							time.get(0), time.get(1), time.get(2));
					
					date = parse(split(timeComponents.get(0), "-"));
					time = parse(split(timeComponents.get(1), ":"));	
					
					LocalDateTime end =  LocalDateTime.of(date.get(0), date.get(1), date.get(2),
							time.get(0), time.get(1), time.get(2));
					activities.add(new MonitoredData(start, end, lineComponents.get(2)));	
				}catch(IndexOutOfBoundsException e1) {
					System.out.println("Line that didn't have enough components was ommited");
				}catch(NumberFormatException e2) {
					System.out.println("Line for which the values didn't respect the types was ommited");
				}
			}	
			buffer.close();
			
		}catch(IOException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
		return activities;
	}
	
	
	public Integer doTask2(List<MonitoredData> activitiesList) {
		Map<Integer, List<MonitoredData>> differentDays = activitiesList
				.stream()
				.collect(Collectors.groupingBy(a->a.getEndTime().getDayOfYear()));
		
		return differentDays.size();
	}
	
	
	public Map<String, Integer> doTask3(List<MonitoredData> activitiesList) {
		
		Map<String, Integer> occurencesOfEachActivity = activitiesList
				.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.reducing(0, e -> 1, Integer::sum)));
		
		return occurencesOfEachActivity;
		
	}

	
	public Map<Integer, Map<String, Integer>> doTask4(List<MonitoredData> activitiesList) {
		
		Map<Integer, Map<String, Integer>> occurencesOfEachActivityPerDay = activitiesList
				.stream()
				.collect(Collectors.groupingBy(a-> a.getStartTime().getDayOfYear(), Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.reducing(0, e -> 1, Integer::sum))));
		
		return occurencesOfEachActivityPerDay;
	}
	
	
	public Map<String, Integer> doTask5(List<MonitoredData> activitiesList) {
		
		Map< String, Integer > eachActivityDuration = activitiesList
					.stream()
					.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(MonitoredData::getDuration)));
		
		return eachActivityDuration;
	
	}
	
	
	public List<String> doTask6(List<MonitoredData> activitiesList) {
		
		int minimDuration = 5 * 60;
		float procent = 0.9f;
		List<String> activitiesFulfillingTheCondition = new ArrayList<String>();
		
		Map<String, Integer> occurencesOfLessThan5Minutes = activitiesList
				.stream()
				.filter(activity -> activity.getDuration() < minimDuration)
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.reducing(0, e -> 1, Integer::sum)));
		
		Map<String, Integer> occurences = doTask3(activitiesList);
		
		for (String key : occurencesOfLessThan5Minutes.keySet()) {
			Integer noOfOccurencesShorterThan5Min = occurencesOfLessThan5Minutes.get(key);
			Integer totalNoOfOccurences = occurences.get(key);
			if(noOfOccurencesShorterThan5Min > totalNoOfOccurences * procent) {
				activitiesFulfillingTheCondition.add(key.toString()+"\n");
			}
		}
		
		return activitiesFulfillingTheCondition;
	}
}
