package business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import data.MonitoredData;
import presentation.FormatParser;
import presentation.OutputFileGenerator;

public class TasksResultProcessor {
	
	private TasksPerformer performer = new TasksPerformer();
	private List<MonitoredData> activities = new ArrayList<MonitoredData>();
	
	private void setActivities(List<MonitoredData> readData) {
		this.activities = readData;
	}
	
	public void task1(String inputFileName) {
		
		List<MonitoredData> activities = performer.doTask1(inputFileName);
		
		if(!activities.isEmpty()) {
			this.setActivities(activities);
			List<String> outputText = new ArrayList<String>();
			outputText.add("The activities are: \n\n");
			FormatParser.monitoredDataListToStringList(activities, outputText);
			OutputFileGenerator.generateOutputFile("Task_1.txt", outputText);
		}
		else {
			System.out.println("There is no activity described in the input file!");
		}
		
	}
	
	
	public void task2() {
		
		if(!this.activities.isEmpty()) {
			Integer result = performer.doTask2(activities);
			List<String> message = new ArrayList<String>();
			message.add("The number of different days is " + result);
			OutputFileGenerator.generateOutputFile("Task_2.txt", message);
		}
		else {
			System.out.println("Cannot do task 2 without task 1 being successfully executed first!");
		}
	}
	
	
	public void task3() {
		
		if(!this.activities.isEmpty()) {
			Map<String, Integer> occurencesOfEachActivity = performer.doTask3(activities);
			List<String> outputText = new ArrayList<String>();
			outputText.add("The number of occurences for each activity is:\n\n");
			FormatParser.mapToStringList(occurencesOfEachActivity, outputText);
			OutputFileGenerator.generateOutputFile("Task_3.txt", outputText);
		}
		else {
			System.out.println("Cannot do task 3 without task 1 being successfully executed first!");
		}
	}
	
	
	public void task4() {
		
		if(!this.activities.isEmpty()) {
			Map<Integer, Map<String, Integer>> occurencesOfEachActivityPerDay = performer.doTask4(activities);
			List<String> outputText = new ArrayList<String>();
			outputText.add("The number of occurences for each activity on each day is:\n\n");
			FormatParser.mapWithValueMapToStringList(occurencesOfEachActivityPerDay, outputText);
			OutputFileGenerator.generateOutputFile("Task_4.txt", outputText);
		}
		else {
			System.out.println("Cannot do task 4 without task 1 being successfully executed first!");
		}
		
	}
	
	
	public void task5() {
		
		if(!this.activities.isEmpty()) {
			Map<String, Integer> eachActivityDuration = performer.doTask5(activities);
			List<String> outputText = new ArrayList<String>();
			outputText.add("The number of seconds for which each activity was performed is:\n\n");
			FormatParser.mapToStringList(eachActivityDuration, outputText);
			OutputFileGenerator.generateOutputFile("Task_5.txt", outputText);
		}
		else {
			System.out.println("Cannot do task 5 without task 1 being successfully executed first!");
		}
	}
	
	
	public void task6() {
		
		if(!this.activities.isEmpty()) {
			List<String> outputText = new ArrayList<String>();
			outputText.add("The activities that fulfill the given condition are:\n\n");
			outputText.addAll(performer.doTask6(activities));
			OutputFileGenerator.generateOutputFile("Task_6.txt", outputText);
		}
		else {
			System.out.println("Cannot do task 6 without task 1 being successfully executed first!");
		}
	}

}
