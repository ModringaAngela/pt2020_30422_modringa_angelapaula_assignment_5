package presentation;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class OutputFileGenerator {
	
	public static void generateOutputFile(String name, List<String> text) {
		try {
			FileWriter file = new FileWriter(name);
			for(String line : text) {
				file.write(line);
				file.flush();
			}
			file.close();
		}catch(IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
}
