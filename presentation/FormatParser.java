package presentation;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import data.MonitoredData;

public class FormatParser {

	public static void monitoredDataListToStringList(List<MonitoredData> activities, List<String> stringList) {
		String line = null;
		for(MonitoredData activity: activities) {
			line = "";
			line+= activity.getActivityLabel().replace("	", "") + "   ";
			line += activity.getStartTime().toString() + "  " + activity.getEndTime().toString() + "\n";
			stringList.add(line);
		}
	}
	
	public static void mapToStringList(Map<String, Integer> map, List<String> stringList){
		String line = null;
		for(Entry<String, Integer> entry: map.entrySet())	{
			line = "";
			line +=entry.getKey().toString().replace("	", "") +":  " +  entry.getValue().toString() + "\n";
			stringList.add(line);
			line = "";
		}
	}
	
	public static void mapWithValueMapToStringList(Map<Integer, Map<String, Integer>> map, List<String> stringList) {
		String line = null;
		for(Entry<Integer, Map<String, Integer>> entry: map.entrySet())	{
			line = "";
			line += "Day " + entry.getKey().toString() + ": ";
			Map<String, Integer> value= entry.getValue();
			for(Entry<String, Integer> imbricatedEntry: value.entrySet()) {
				line += imbricatedEntry.getKey().toString().replace("	", "") +" "+ imbricatedEntry.getValue().toString()+"; ";
			}
			line += "\n";
			stringList.add(line);
			line = "";
		}
	}
	
}
