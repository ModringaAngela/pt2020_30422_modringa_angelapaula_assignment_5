package data;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class MonitoredData {
	
	LocalDateTime startTime;
	LocalDateTime endTime;
	String activityLabel;
	
	public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activityLabel) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	public Integer getDuration() {
		
		long seconds = ChronoUnit.SECONDS.between(this.startTime, this.endTime);
		Integer result = (int)(long) seconds;
		
		return result;
		
	}
	
}
